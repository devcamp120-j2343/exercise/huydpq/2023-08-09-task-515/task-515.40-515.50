import 'bootstrap/dist/css/bootstrap.min.css'
import TittleComponent from './Components/formComponent/tittleComponent';
import FormComponent from './Components/tittleComponent/formComponent';
import "./App.css"
function App() {
  return (
    <div className="container mt-4">
      <TittleComponent/>
      <FormComponent/>
    </div>
  );
}

export default App;
