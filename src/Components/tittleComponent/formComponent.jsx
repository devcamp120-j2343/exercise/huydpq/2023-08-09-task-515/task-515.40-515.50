import { Component } from "react";

class FormComponent extends Component {
    submit(event){
        console.log("Sumit dược ấn")
        event.preventDefault();
    };
    handleChange = (event) => {
       console.log(event.target.value);
      };
    render() {
        return (
            <div className="form mt-4">
                <div className="">
                    <form onSubmit={this.submit} action="">
                        <div className="row mt-3">
                            <div className="col-sm-2">
                                <label htmlFor="">FirstName</label>
                            </div>
                            <div className="col-md-10 ">
                                <input className="form-control" placeholder="First Name" type="text" />
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-sm-2">
                                <label htmlFor="">Lastname</label>
                            </div>
                            <div className="col-md-10 ">
                                <input className="form-control" placeholder="Last Name" type="text" />
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-sm-2">
                                <label htmlFor="">Coutry</label>
                            </div>
                            <div className="col-md-10">
                                <select onChange={this.handleChange} className="form-control">
                                    <option>Australia</option>
                                    <option>Việt Name</option>
                                    <option>China</option>
                                </select>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-sm-2">
                                <label htmlFor="">Subject</label>
                            </div>
                            <div className="col-md-10 ">
                                <textarea rows="8" className="form-control"></textarea>
                            </div>
                        </div>
                        <div className="row my-3">
                            <div className="col-sm-12">
                               <button type="submit" className="">Send data</button>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default FormComponent